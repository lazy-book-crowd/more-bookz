For adding newa authors on the reading club please provide a book before asking to add the author,
in the past most people stopped doing a book and thus quite some auhtors do not even have a single book.

## All Our Authors (2016-01-28)

    # | Sort name | Full name and link | Books on the reading club
 ---: | :-------- | :----------------: | ------: 
 1 | Doctorow | [Cory Doctorow](http://c3jemx2ube5v5zpg.onion/?author=view&id=1) | { 16 }
 2 | Asimov | [Isaac Asimov](http://c3jemx2ube5v5zpg.onion/?author=view&id=2) | { 30 }
 3 | Raymond | [Eric Steven Raymond](http://c3jemx2ube5v5zpg.onion/?author=view&id=3) | { 0 }
 4 | Adams | [Douglas Noël Adams](http://c3jemx2ube5v5zpg.onion/?author=view&id=4) | { 7 }
 5 | Heinlein | [Robert Anson Heinlein](http://c3jemx2ube5v5zpg.onion/?author=view&id=5) | { 26 }
 6 | Clarke | [Sir Arthur Charles Clarke](http://c3jemx2ube5v5zpg.onion/?author=view&id=6) | { 5 }
 7 | Bakunin | [Mikhail Alexandrovich Bakunin](http://c3jemx2ube5v5zpg.onion/?author=view&id=7) | { 2 }
 8 | Proudhon | [Pierre-Joseph Proudhon](http://c3jemx2ube5v5zpg.onion/?author=view&id=8) | { 2 }
 9 | Verne | [Jules Gabriel Verne](http://c3jemx2ube5v5zpg.onion/?author=view&id=9) | { 2 }
 10 | Russell | [Eric Frank Russell](http://c3jemx2ube5v5zpg.onion/?author=view&id=10) | { 2 }
 11 | Lovecraft | [Howard Phillips Lovecraft](http://c3jemx2ube5v5zpg.onion/?author=view&id=11) | { 3 }
 12 | Stoker | [Abraham Stoker](http://c3jemx2ube5v5zpg.onion/?author=view&id=12) | { 0 }
 13 | Tolkien | [John Ronald Reuel Tolkien](http://c3jemx2ube5v5zpg.onion/?author=view&id=13) | { 7 }
 14 | Laumer | [John Keith Laumer](http://c3jemx2ube5v5zpg.onion/?author=view&id=14) | { 18 }
 15 | Piper | [Henry Beam Piper](http://c3jemx2ube5v5zpg.onion/?author=view&id=15) | { 20 }
 16 | Wells | [Herbert George Wells](http://c3jemx2ube5v5zpg.onion/?author=view&id=16) | { 2 }
 17 | Norton | [Alice Mary Norton](http://c3jemx2ube5v5zpg.onion/?author=view&id=17) | { 11 }
 18 | Blair | [Eric Arthur Blair](http://c3jemx2ube5v5zpg.onion/?author=view&id=18) | { 11 }
 19 | II | [William Seward Burroughs II](http://c3jemx2ube5v5zpg.onion/?author=view&id=19) | { 0 }
 20 | Gibson | [William Ford Gibson](http://c3jemx2ube5v5zpg.onion/?author=view&id=20) | { 10 }
 21 | Poe | [Edgar Allan Poe](http://c3jemx2ube5v5zpg.onion/?author=view&id=21) | { 0 }
 22 | Dick | [Philip Kindred Dick](http://c3jemx2ube5v5zpg.onion/?author=view&id=22) | { 50 }
 23 | Rathsack | [Thomas Rathsack](http://c3jemx2ube5v5zpg.onion/?author=view&id=23) | { 0 }
 24 | Flint | [Eric Flint](http://c3jemx2ube5v5zpg.onion/?author=view&id=24) | { 19 }
 25 | Drake | [David Drake](http://c3jemx2ube5v5zpg.onion/?author=view&id=25) | { 25 }
 26 | Doyle | [Sir Arthur Ignatius Conan Doyle](http://c3jemx2ube5v5zpg.onion/?author=view&id=26) | { 6 }
 27 | Kaczynski | [Theodore John Kaczynski](http://c3jemx2ube5v5zpg.onion/?author=view&id=27) | { 0 }
 28 | Stross | [Charles David George Stross](http://c3jemx2ube5v5zpg.onion/?author=view&id=28) | { 3 }
 29 | Tarpley | [Webster Griffin Tarpley](http://c3jemx2ube5v5zpg.onion/?author=view&id=29) | { 0 }
 30 | Russell | [Bertrand Russell](http://c3jemx2ube5v5zpg.onion/?author=view&id=30) | { 2 }
 31 | Gellis | [Roberta Gellis](http://c3jemx2ube5v5zpg.onion/?author=view&id=31) | { 2 }
 32 | Rand | [Ayn Rand](http://c3jemx2ube5v5zpg.onion/?author=view&id=32) | { 3 }
 33 | Lippmann | [Walter Lippmann](http://c3jemx2ube5v5zpg.onion/?author=view&id=33) | { 0 }
 34 | Ringo | [John Ringo](http://c3jemx2ube5v5zpg.onion/?author=view&id=34) | { 25 }
 35 | Weber | [David Mark Weber](http://c3jemx2ube5v5zpg.onion/?author=view&id=35) | { 32 }
 36 | Bookchin | [Murray Bookchin](http://c3jemx2ube5v5zpg.onion/?author=view&id=36) | { 2 }
 37 | Watts | [Peter Watts](http://c3jemx2ube5v5zpg.onion/?author=view&id=37) | { 3 }
 38 | Sterling | [Michael Bruce Sterling](http://c3jemx2ube5v5zpg.onion/?author=view&id=38) | { 0 }
 39 | Jenkins | [William Fitzgerald Jenkins](http://c3jemx2ube5v5zpg.onion/?author=view&id=39) | { 6 }
 40 | Williamson | [Michael  Z. Williamson](http://c3jemx2ube5v5zpg.onion/?author=view&id=40) | { 2 }
 41 | Rucker | [Rudolf von Bitter Rucker](http://c3jemx2ube5v5zpg.onion/?author=view&id=41) | { 3 }
 42 | Brotherton | [Michael Brotherton](http://c3jemx2ube5v5zpg.onion/?author=view&id=42) | { 0 }
 43 | Peake | [Mervyn Laurence Peake](http://c3jemx2ube5v5zpg.onion/?author=view&id=43) | { 3 }
 44 | Huxley | [Aldous Leonard Huxley](http://c3jemx2ube5v5zpg.onion/?author=view&id=44) | { 2 }
 45 | Crichton | [John Michael Crichton](http://c3jemx2ube5v5zpg.onion/?author=view&id=45) | { 2 }
 46 | Silva | [A. L. De Silva](http://c3jemx2ube5v5zpg.onion/?author=view&id=46) | { 0 }
 47 | Lessig | [Lawrence Lessig](http://c3jemx2ube5v5zpg.onion/?author=view&id=47) | { 3 }
 48 | Bujold | [Lois McMaster Bujold](http://c3jemx2ube5v5zpg.onion/?author=view&id=48) | { 9 }
 49 | Kerouac | [Jean-Louis Kerouac](http://c3jemx2ube5v5zpg.onion/?author=view&id=49) | { 2 }
 50 | Goldman | [Emma Goldman](http://c3jemx2ube5v5zpg.onion/?author=view&id=50) | { 2 }
 51 | Barker | [Clive Barker](http://c3jemx2ube5v5zpg.onion/?author=view&id=51) | { 6 }
 52 | Taylor | [Travis Shane Taylor](http://c3jemx2ube5v5zpg.onion/?author=view&id=52) | { 4 }
 53 | Guerber | [Hélène Adeline Guerber](http://c3jemx2ube5v5zpg.onion/?author=view&id=53) | { 0 }
 54 | Boyle | [James Boyle](http://c3jemx2ube5v5zpg.onion/?author=view&id=54) | { 0 }
 55 | Rasmussen | [Knud Johan Victor Rasmussen](http://c3jemx2ube5v5zpg.onion/?author=view&id=55) | { 0 }
 56 | Harrison | [Harry Harrison](http://c3jemx2ube5v5zpg.onion/?author=view&id=56) | { 3 }
 57 | Crosby | [Harry Christopher Crosby](http://c3jemx2ube5v5zpg.onion/?author=view&id=57) | { 6 }
 58 | Stallman | [Richard Matthew Stallman](http://c3jemx2ube5v5zpg.onion/?author=view&id=58) | { 0 }
 59 | Jr. | [John Frederick Lange, Jr.](http://c3jemx2ube5v5zpg.onion/?author=view&id=59) | { 4 }
 60 | Bukowski | [Henry Charles Bukowski](http://c3jemx2ube5v5zpg.onion/?author=view&id=60) | { 2 }
 61 | Leighton | [Robert Leighton](http://c3jemx2ube5v5zpg.onion/?author=view&id=61) | { 0 }
 62 | Niven | [Laurence van Cott Niven](http://c3jemx2ube5v5zpg.onion/?author=view&id=62) | { 5 }
 63 | Marx | [Karl Heinrich Marx](http://c3jemx2ube5v5zpg.onion/?author=view&id=63) | { 2 }
 64 | Franzen | [Jonathan Franzen](http://c3jemx2ube5v5zpg.onion/?author=view&id=64) | { 0 }
 65 | Bradbury | [Ray Douglas Bradbury](http://c3jemx2ube5v5zpg.onion/?author=view&id=65) | { 2 }
 66 | Burroughs | [Edgar Rice Burroughs](http://c3jemx2ube5v5zpg.onion/?author=view&id=66) | { 8 }
 67 | Jr. | [John Wood Campbell, Jr.](http://c3jemx2ube5v5zpg.onion/?author=view&id=67) | { 2 }
 68 | Lackey | [Mercedes Lackey](http://c3jemx2ube5v5zpg.onion/?author=view&id=68) | { 7 }
 69 | Engström | [Lars Christian Engström](http://c3jemx2ube5v5zpg.onion/?author=view&id=69) | { 0 }
 70 | Allen | [Roger MacBride Allen](http://c3jemx2ube5v5zpg.onion/?author=view&id=70) | { 3 }
 71 | Wu | [William F. Wu](http://c3jemx2ube5v5zpg.onion/?author=view&id=71) | { 6 }
 72 | Rey | [Lester del Rey](http://c3jemx2ube5v5zpg.onion/?author=view&id=72) | { 2 }
 73 | Bradley | [Marion Eleanor Zimmer Bradley](http://c3jemx2ube5v5zpg.onion/?author=view&id=73) | { 0 }
 74 | Smith | [George Oliver Smith](http://c3jemx2ube5v5zpg.onion/?author=view&id=74) | { 2 }
 75 | Tiedemann | [Mark William Tiedemann](http://c3jemx2ube5v5zpg.onion/?author=view&id=75) | { 2 }
 76 | Smith | [Edward Elmer Smith](http://c3jemx2ube5v5zpg.onion/?author=view&id=76) | { 0 }
 77 | Sagan | [Carl Edward Sagan](http://c3jemx2ube5v5zpg.onion/?author=view&id=77) | { 2 }
 78 | King | [Stephen Edwin King](http://c3jemx2ube5v5zpg.onion/?author=view&id=78) | { 5 }
 79 | Thompson | [Hunter Stockton Thompson](http://c3jemx2ube5v5zpg.onion/?author=view&id=79) | { 0 }
 80 | Schmitz | [James Henry Schmitz](http://c3jemx2ube5v5zpg.onion/?author=view&id=80) | { 6 }
 81 | Graeber | [David Rolfe Graeber](http://c3jemx2ube5v5zpg.onion/?author=view&id=81) | { 0 }
 82 | Wolff | [Robert Paul Wolff](http://c3jemx2ube5v5zpg.onion/?author=view&id=82) | { 0 }
 83 | Levy | [Steven Levy](http://c3jemx2ube5v5zpg.onion/?author=view&id=83) | { 3 }
 84 | Link | [Kelly Link](http://c3jemx2ube5v5zpg.onion/?author=view&id=84) | { 0 }
 85 | Schmidt | [Johann Kaspar Schmidt](http://c3jemx2ube5v5zpg.onion/?author=view&id=85) | { 0 }
 86 | Jr. | [Robert Charles Black, Jr.](http://c3jemx2ube5v5zpg.onion/?author=view&id=86) | { 0 }
 87 | Stirling | [Stephen Michael Stirling](http://c3jemx2ube5v5zpg.onion/?author=view&id=87) | { 3 }
 88 | Colfer | [Eoin Colfer](http://c3jemx2ube5v5zpg.onion/?author=view&id=88) | { 10 }
 89 | Jr. | [Kurt Vonnegut, Jr.](http://c3jemx2ube5v5zpg.onion/?author=view&id=89) | { 7 }
 90 | Ludlum | [Robert Ludlum](http://c3jemx2ube5v5zpg.onion/?author=view&id=90) | { 0 }
 91 | Koontz | [Dean Ray Koontz](http://c3jemx2ube5v5zpg.onion/?author=view&id=91) | { 2 }
 92 | Ellis | [Bret Easton Ellis](http://c3jemx2ube5v5zpg.onion/?author=view&id=92) | { 0 }
 93 | Stephenson | [Neal Town Stephenson](http://c3jemx2ube5v5zpg.onion/?author=view&id=93) | { 2 }
 94 | Larsson | [Karl Stig-Erland Larsson](http://c3jemx2ube5v5zpg.onion/?author=view&id=94) | { 3 }
 95 | Unknown | [Unknown](http://c3jemx2ube5v5zpg.onion/?author=view&id=95) | { 0 }
 96 | Everett-Green | [Evelyn Ward Everett-Green](http://c3jemx2ube5v5zpg.onion/?author=view&id=96) | { 0 }
 97 | Black | [Edwin Black](http://c3jemx2ube5v5zpg.onion/?author=view&id=97) | { 0 }
 98 | Brown | [Dan Brown](http://c3jemx2ube5v5zpg.onion/?author=view&id=98) | { 0 }
 99 | Jr. | [Franklin Patrick Herbert, Jr.](http://c3jemx2ube5v5zpg.onion/?author=view&id=99) | { 6 }
 100 | Brooks | [Maximillian Michael Brooks](http://c3jemx2ube5v5zpg.onion/?author=view&id=100) | { 0 }
 101 | McChesney | [Robert Waterman McChesney](http://c3jemx2ube5v5zpg.onion/?author=view&id=101) | { 0 }
 102 | Rowling | [Joanne Rowling](http://c3jemx2ube5v5zpg.onion/?author=view&id=102) | { 17 }
 103 | Herbert | [Brian Patrick Herbert](http://c3jemx2ube5v5zpg.onion/?author=view&id=103) | { 3 }
 104 | Ward | [Colin Ward](http://c3jemx2ube5v5zpg.onion/?author=view&id=104) | { 0 }
 105 | Falkvinge | [Rickard Falkvinge](http://c3jemx2ube5v5zpg.onion/?author=view&id=105) | { 0 }
 106 | Rossi | [Luigi Rossi](http://c3jemx2ube5v5zpg.onion/?author=view&id=106) | { 0 }
 107 | Diamond | [Jared Mason Diamond](http://c3jemx2ube5v5zpg.onion/?author=view&id=107) | { 0 }
 108 | Heller | [Joseph Heller](http://c3jemx2ube5v5zpg.onion/?author=view&id=108) | { 0 }
 109 | Borges | [Jorge Francisco Isidoro Luis Borges](http://c3jemx2ube5v5zpg.onion/?author=view&id=109) | { 0 }
 110 | Bova | [Benjamin William Bova](http://c3jemx2ube5v5zpg.onion/?author=view&id=110) | { 2 }
 111 | Collins | [Suzanne Collins](http://c3jemx2ube5v5zpg.onion/?author=view&id=111) | { 3 }
 112 | Card | [Orson Scott Card](http://c3jemx2ube5v5zpg.onion/?author=view&id=112) | { 0 }
 113 | Mill | [John Stuart Mill](http://c3jemx2ube5v5zpg.onion/?author=view&id=113) | { 2 }
 114 | Camus | [Albert Camus](http://c3jemx2ube5v5zpg.onion/?author=view&id=114) | { 2 }
 115 | Golding | [Sir William Gerald Golding](http://c3jemx2ube5v5zpg.onion/?author=view&id=115) | { 0 }
 116 | Carson | [Kevin Amos Carson](http://c3jemx2ube5v5zpg.onion/?author=view&id=116) | { 0 }
 117 | Fourest | [Caroline Fourest](http://c3jemx2ube5v5zpg.onion/?author=view&id=117) | { 0 }
 118 | Greenwald | [Glenn Greenwald](http://c3jemx2ube5v5zpg.onion/?author=view&id=118) | { 2 }
 119 | Greene | [Robert Greene](http://c3jemx2ube5v5zpg.onion/?author=view&id=119) | { 0 }
 120 | Tzu | [Sun Tzu](http://c3jemx2ube5v5zpg.onion/?author=view&id=120) | { 0 }
 121 | Jones | [John Robert Jones](http://c3jemx2ube5v5zpg.onion/?author=view&id=121) | { 2 }
 122 | Keyes | [Daniel Keyes](http://c3jemx2ube5v5zpg.onion/?author=view&id=122) | { 0 }
 123 | Dawkins | [Clinton Richard Dawkins](http://c3jemx2ube5v5zpg.onion/?author=view&id=123) | { 0 }
 124 | Hofstadter | [Douglas Richard Hofstadter](http://c3jemx2ube5v5zpg.onion/?author=view&id=124) | { 0 }
 125 | Ruen | [Chris Ruen](http://c3jemx2ube5v5zpg.onion/?author=view&id=125) | { 0 }
 126 | King | [Joseph Hillstrom King](http://c3jemx2ube5v5zpg.onion/?author=view&id=126) | { 0 }
 127 | Balko | [Radley Balko](http://c3jemx2ube5v5zpg.onion/?author=view&id=127) | { 0 }
 128 | Mead | [Richelle Mead](http://c3jemx2ube5v5zpg.onion/?author=view&id=128) | { 12 }
 129 | Hillers | [Marta Hillers](http://c3jemx2ube5v5zpg.onion/?author=view&id=129) | { 0 }
 130 | Stroud | [Jonathan Anthony Stroud](http://c3jemx2ube5v5zpg.onion/?author=view&id=130) | { 0 }
 131 | Coleman | [Enid Gabriella Coleman](http://c3jemx2ube5v5zpg.onion/?author=view&id=131) | { 2 }
 132 | Cialdini | [Robert Cialdini](http://c3jemx2ube5v5zpg.onion/?author=view&id=132) | { 2 }
 133 | Martin | [George Raymond Richard Martin](http://c3jemx2ube5v5zpg.onion/?author=view&id=133) | { 8 }
 134 | Roth | [Veronica Roth](http://c3jemx2ube5v5zpg.onion/?author=view&id=134) | { 3 }
 135 | Rumelt | [Judith Rumelt](http://c3jemx2ube5v5zpg.onion/?author=view&id=135) | { 6 }
 136 | Gabaldon | [Diana J. Gabaldon](http://c3jemx2ube5v5zpg.onion/?author=view&id=136) | { 8 }
 137 | Weir | [Andy Weir](http://c3jemx2ube5v5zpg.onion/?author=view&id=137) | { 2 }
 138 | Keegan | [Marina Evelyn Keegan](http://c3jemx2ube5v5zpg.onion/?author=view&id=138) | { 0 }
 139 | Brown | [Pierce Brown](http://c3jemx2ube5v5zpg.onion/?author=view&id=139) | { 0 }
 140 | Rice | [Anne Rice](http://c3jemx2ube5v5zpg.onion/?author=view&id=140) | { 2 }
 141 | Rowell | [Rainbow Rowell](http://c3jemx2ube5v5zpg.onion/?author=view&id=141) | { 0 }
 142 | Klein | [Naomi Klein](http://c3jemx2ube5v5zpg.onion/?author=view&id=142) | { 0 }
 143 | Forster | [Edward Morgan Forster](http://c3jemx2ube5v5zpg.onion/?author=view&id=143) | { 0 }
 144 | Burnett | [Frances Eliza Hodgson Burnett](http://c3jemx2ube5v5zpg.onion/?author=view&id=144) | { 0 }
 145 | Blyton | [Enid Mary Blyton](http://c3jemx2ube5v5zpg.onion/?author=view&id=145) | { 5 }
 146 | Green | [John Michael Green](http://c3jemx2ube5v5zpg.onion/?author=view&id=146) | { 2 }
 147 | Tolstoy | [Lev Nikolayevich Tolstoy](http://c3jemx2ube5v5zpg.onion/?author=view&id=147) | { 0 }
 148 | Jr. | [Richard Russell Riordan, Jr.](http://c3jemx2ube5v5zpg.onion/?author=view&id=148) | { 5 }
 149 | Guin | [Ursula Kroeber Le Guin](http://c3jemx2ube5v5zpg.onion/?author=view&id=149) | { 5 }
 150 | Lee | [Nelle Harper Lee](http://c3jemx2ube5v5zpg.onion/?author=view&id=150) | { 4 }
 151 | Kafka | [Franz Kafka](http://c3jemx2ube5v5zpg.onion/?author=view&id=151) | { 0 }
 152 | Moye | [Guan Moye](http://c3jemx2ube5v5zpg.onion/?author=view&id=152) | { 2 }
 153 | Lindqvist | [John Ajvide Lindqvist](http://c3jemx2ube5v5zpg.onion/?author=view&id=153) | { 0 }
 154 | Martin | [Billy Martin](http://c3jemx2ube5v5zpg.onion/?author=view&id=154) | { 0 }
 155 | Pullman | [Philip Pullman](http://c3jemx2ube5v5zpg.onion/?author=view&id=155) | { 3 }
 156 | Roberts | [Nora Roberts](http://c3jemx2ube5v5zpg.onion/?author=view&id=156) | { 0 }
 157 | fils | [Alexandre Dumas, fils](http://c3jemx2ube5v5zpg.onion/?author=view&id=157) | { 0 }
 158 | Saavedra | [Miguel de Cervantes Saavedra](http://c3jemx2ube5v5zpg.onion/?author=view&id=158) | { 0 }
 159 | Butcher | [Jim Butcher](http://c3jemx2ube5v5zpg.onion/?author=view&id=159) | { 0 }
 160 | Hawkins | [Rachel Hawkins](http://c3jemx2ube5v5zpg.onion/?author=view&id=160) | { 0 }
 161 | Estep | [Jennifer Estep](http://c3jemx2ube5v5zpg.onion/?author=view&id=161) | { 0 }
 162 | Conrad | [Joseph Conrad](http://c3jemx2ube5v5zpg.onion/?author=view&id=162) | { 0 }
 163 | Brontë | [Charlotte Brontë](http://c3jemx2ube5v5zpg.onion/?author=view&id=163) | { 0 }
 164 | Aster | [Christian von Aster](http://c3jemx2ube5v5zpg.onion/?author=view&id=164) | { 0 }
 165 | Martel | [Yann Martel](http://c3jemx2ube5v5zpg.onion/?author=view&id=165) | { 5 }
 166 | Bowden | [Mark Robert Bowden](http://c3jemx2ube5v5zpg.onion/?author=view&id=166) | { 2 }
 167 | Baldwin | [James Arthur Baldwin](http://c3jemx2ube5v5zpg.onion/?author=view&id=167) | { 2 }
 168 | Assange | [Julian Paul Assange](http://c3jemx2ube5v5zpg.onion/?author=view&id=168) | { 2 }
 169 | Williams | [Sam Williams](http://c3jemx2ube5v5zpg.onion/?author=view&id=169) | { 0 }
 170 | Christie | [Dame Agatha Mary Clarissa Christie](http://c3jemx2ube5v5zpg.onion/?author=view&id=170) | { 0 }
 171 | Zusak | [Markus Frank Zusak](http://c3jemx2ube5v5zpg.onion/?author=view&id=171) | { 2 }
 172 | Jr. | [William James O'Reilly, Jr.](http://c3jemx2ube5v5zpg.onion/?author=view&id=172) | { 0 }
 173 | Funke | [Cornelia Maria Funke](http://c3jemx2ube5v5zpg.onion/?author=view&id=173) | { 2 }
 174 | Weeks | [Brent Weeks](http://c3jemx2ube5v5zpg.onion/?author=view&id=174) | { 4 }
 175 | Jr | [James Oliver Rigney, Jr](http://c3jemx2ube5v5zpg.onion/?author=view&id=175) | { 6 }
 176 | Abbott | [Edwin Abbott Abbott](http://c3jemx2ube5v5zpg.onion/?author=view&id=176) | { 2 }
 177 | Gordon | [Kim Althea Gordon](http://c3jemx2ube5v5zpg.onion/?author=view&id=177) | { 0 }
 178 | Torvalds | [Linus Benedict Torvalds](http://c3jemx2ube5v5zpg.onion/?author=view&id=178) | { 0 }
 179 | Landy | [Derek Landy](http://c3jemx2ube5v5zpg.onion/?author=view&id=179) | { 0 }
 180 | Olson | [Parmy Olson](http://c3jemx2ube5v5zpg.onion/?author=view&id=180) | { 0 }
 181 | Hosseini | [Khaled Hosseini](http://c3jemx2ube5v5zpg.onion/?author=view&id=181) | { 0 }
 182 | Austen | [Jane Austen](http://c3jemx2ube5v5zpg.onion/?author=view&id=182) | { 0 }
 183 | Alcott | [Louisa May Alcott](http://c3jemx2ube5v5zpg.onion/?author=view&id=183) | { 6 }
 184 | Sparks | [Nicholas Charles Sparks](http://c3jemx2ube5v5zpg.onion/?author=view&id=184) | { 17 }
 185 | Pratchett | [Sir Terence David John Pratchett](http://c3jemx2ube5v5zpg.onion/?author=view&id=185) | { 46 }
 186 | Remnick | [David Remnick](http://c3jemx2ube5v5zpg.onion/?author=view&id=186) | { 0 }
 187 | Himes | [Chester Bomar Himes](http://c3jemx2ube5v5zpg.onion/?author=view&id=187) | { 0 }
 188 | Schneier | [Bruce Schneier](http://c3jemx2ube5v5zpg.onion/?author=view&id=188) | { 2 }
 189 | Schlink | [Bernhard Schlink](http://c3jemx2ube5v5zpg.onion/?author=view&id=189) | { 2 }
 190 | Kurz | [Constanze Kurz](http://c3jemx2ube5v5zpg.onion/?author=view&id=190) | { 2 }
 191 | Rieger | [Frank Rieger](http://c3jemx2ube5v5zpg.onion/?author=view&id=191) | { 0 }
 192 | Lewis | [Clive Staples Lewis](http://c3jemx2ube5v5zpg.onion/?author=view&id=192) | { 3 }
 193 | Felscherinow | [Vera Christiane Felscherinow](http://c3jemx2ube5v5zpg.onion/?author=view&id=193) | { 2 }
 194 | Paolini | [Christopher James Paolini](http://c3jemx2ube5v5zpg.onion/?author=view&id=194) | { 0 }
 195 | Beckedahl | [Markus Beckedahl](http://c3jemx2ube5v5zpg.onion/?author=view&id=195) | { 6 }
 196 | Roy | [Suzanna Arundhati Roy](http://c3jemx2ube5v5zpg.onion/?author=view&id=196) | { 0 }
 197 | Jonasson | [Pär-Ola Jonas Jonasson](http://c3jemx2ube5v5zpg.onion/?author=view&id=197) | { 0 }
 198 | Kuhn | [Thomas Samuel Kuhn](http://c3jemx2ube5v5zpg.onion/?author=view&id=198) | { 0 }
 199 | Kling | [Marc-Uwe Kling](http://c3jemx2ube5v5zpg.onion/?author=view&id=199) | { 3 }
 200 | DePrima | [Thomas J DePrima](http://c3jemx2ube5v5zpg.onion/?author=view&id=200) | { 0 }
 201 | Brown | [Dale Brown](http://c3jemx2ube5v5zpg.onion/?author=view&id=201) | { 0 }
 202 | Force | [Marie Force](http://c3jemx2ube5v5zpg.onion/?author=view&id=202) | { 0 }
 203 | Cox | [Chloe Cox](http://c3jemx2ube5v5zpg.onion/?author=view&id=203) | { 0 }
 204 | Stevenson | [Robert Louis Balfour Stevenson](http://c3jemx2ube5v5zpg.onion/?author=view&id=204) | { 0 }
 205 | Lewis | [Sir George Cornewall Lewis](http://c3jemx2ube5v5zpg.onion/?author=view&id=205) | { 0 }
 206 | Grassmuck | [Volker Grassmuck](http://c3jemx2ube5v5zpg.onion/?author=view&id=206) | { 0 }
 207 | Müller | [Dirk Müller](http://c3jemx2ube5v5zpg.onion/?author=view&id=207) | { 0 }
 208 | Remarque | [Erich Maria Remarque](http://c3jemx2ube5v5zpg.onion/?author=view&id=208) | { 2 }
 209 | Rollet-Andriane | [Marayat Rollet-Andriane](http://c3jemx2ube5v5zpg.onion/?author=view&id=209) | { 7 }
 210 | Brogan | [Denis Hugh Vercingetorix Brogan](http://c3jemx2ube5v5zpg.onion/?author=view&id=210) | { 0 }
 211 | Fisher | [Mark Fisher](http://c3jemx2ube5v5zpg.onion/?author=view&id=211) | { 0 }
 212 | Benson | [Raymond Benson](http://c3jemx2ube5v5zpg.onion/?author=view&id=212) | { 0 }
 213 | Dietz | [William Corey Dietz](http://c3jemx2ube5v5zpg.onion/?author=view&id=213) | { 0 }
 214 | Fitzgerald | [Francis Scott Key Fitzgerald](http://c3jemx2ube5v5zpg.onion/?author=view&id=214) | { 0 }
 215 | Coelho | [Paulo Coelho](http://c3jemx2ube5v5zpg.onion/?author=view&id=215) | { 4 }
 216 | Rusling | [David A. Rusling](http://c3jemx2ube5v5zpg.onion/?author=view&id=216) | { 0 }
 217 | Harris | [Thomas Harris](http://c3jemx2ube5v5zpg.onion/?author=view&id=217) | { 0 }
 218 | Dobbs | [Michael Dobbs](http://c3jemx2ube5v5zpg.onion/?author=view&id=218) | { 5 }
 219 | Süskind | [Patrick Süskind](http://c3jemx2ube5v5zpg.onion/?author=view&id=219) | { 2 }
 220 | Kästner | [Emil Erich Kästner](http://c3jemx2ube5v5zpg.onion/?author=view&id=220) | { 0 }
 221 | Grass | [Günter Wilhelm Grass](http://c3jemx2ube5v5zpg.onion/?author=view&id=221) | { 6 }
 222 | Sanderson | [Brandon Sanderson](http://c3jemx2ube5v5zpg.onion/?author=view&id=222) | { 0 }
 223 | Reynolds | [Alastair Preston Reynolds](http://c3jemx2ube5v5zpg.onion/?author=view&id=223) | { 0 }
 224 | Stöcker | [Christian Stöcker](http://c3jemx2ube5v5zpg.onion/?author=view&id=224) | { 0 }
 225 | Frisch | [Max Rudolf Frisch](http://c3jemx2ube5v5zpg.onion/?author=view&id=225) | { 2 }
 226 | Ryan | [Christopher Ryan](http://c3jemx2ube5v5zpg.onion/?author=view&id=226) | { 0 }
 227 | Wodehouse | [Sir Pelham Grenville Wodehouse](http://c3jemx2ube5v5zpg.onion/?author=view&id=227) | { 0 }
 228 | York | [Jillian C. York](http://c3jemx2ube5v5zpg.onion/?author=view&id=228) | { 0 }
 229 | Albrecht | [Jan Philipp Albrecht](http://c3jemx2ube5v5zpg.onion/?author=view&id=229) | { 0 }
 230 | Schaar | [Peter Schaar](http://c3jemx2ube5v5zpg.onion/?author=view&id=230) | { 3 }
 231 | Cline | [Ernest Christy Cline](http://c3jemx2ube5v5zpg.onion/?author=view&id=231) | { 2 }
 232 | Preußler | [Otfried Preußler](http://c3jemx2ube5v5zpg.onion/?author=view&id=232) | { 2 }
 233 | Constantine | [Robin Constantine](http://c3jemx2ube5v5zpg.onion/?author=view&id=233) | { 0 }
 234 | Hülshoff | [Anna Elisabeth Franziska Adolphine Wilhelmine Louise Maria, Freiin von Droste zu Hülshoff](http://c3jemx2ube5v5zpg.onion/?author=view&id=234) | { 0 }
 235 | Grasse | [Christian Grasse](http://c3jemx2ube5v5zpg.onion/?author=view&id=235) | { 0 }
 236 | Maher | [Katherine Maher](http://c3jemx2ube5v5zpg.onion/?author=view&id=236) | { 0 }
 237 | Eco | [Umberto Eco](http://c3jemx2ube5v5zpg.onion/?author=view&id=237) | { 0 }
 238 | Scahill | [Jeremy Scahill](http://c3jemx2ube5v5zpg.onion/?author=view&id=238) | { 0 }
 239 | Mazzetti | [Mark Mazzetti](http://c3jemx2ube5v5zpg.onion/?author=view&id=239) | { 0 }
 240 | Mann | [Luiz Heinrich Mann](http://c3jemx2ube5v5zpg.onion/?author=view&id=240) | { 0 }
 241 | Simons | [Martin Simons](http://c3jemx2ube5v5zpg.onion/?author=view&id=241) | { 0 }
 242 | Nizon | [Paul Nizon](http://c3jemx2ube5v5zpg.onion/?author=view&id=242) | { 0 }
 243 | Domscheit-Berg | [Anke Margarete Domscheit-Berg](http://c3jemx2ube5v5zpg.onion/?author=view&id=243) | { 0 }
 244 | Zweig | [Stefan Zweig](http://c3jemx2ube5v5zpg.onion/?author=view&id=244) | { 0 }
 245 | Wilde | [Oscar Fingal O'Flahertie Wills Wilde](http://c3jemx2ube5v5zpg.onion/?author=view&id=245) | { 0 }
 246 | Neville | [Katherine Neville](http://c3jemx2ube5v5zpg.onion/?author=view&id=246) | { 0 }
 247 | Graham | [Paul Graham](http://c3jemx2ube5v5zpg.onion/?author=view&id=247) | { 0 }
 248 | Irving | [Washington Irving](http://c3jemx2ube5v5zpg.onion/?author=view&id=248) | { 3 }
 249 | Strasser | [Todd Strasser](http://c3jemx2ube5v5zpg.onion/?author=view&id=249) | { 0 }
 250 | Noack | [Hans-Georg Noack](http://c3jemx2ube5v5zpg.onion/?author=view&id=250) | { 0 }
 251 | Gibran | [Khalil Gibran](http://c3jemx2ube5v5zpg.onion/?author=view&id=251) | { 0 }
 252 | Milne | [Alan Alexander Milne](http://c3jemx2ube5v5zpg.onion/?author=view&id=252) | { 0 }
 253 | Dass | [Ram Dass](http://c3jemx2ube5v5zpg.onion/?author=view&id=253) | { 0 }
 254 | Lagercrantz | [David Lagercrantz](http://c3jemx2ube5v5zpg.onion/?author=view&id=254) | { 0 }
 255 | Vermes | [Timur Vermes](http://c3jemx2ube5v5zpg.onion/?author=view&id=255) | { 0 }
 256 | Hücking | [Birgit Hücking](http://c3jemx2ube5v5zpg.onion/?author=view&id=256) | { 0 }
 257 | Feynman | [Richard Phillips Feynman](http://c3jemx2ube5v5zpg.onion/?author=view&id=257) | { 0 }
 258 | Leighton | [Ralph Leighton](http://c3jemx2ube5v5zpg.onion/?author=view&id=258) | { 0 }
 259 | Johns | [Adrian Johns](http://c3jemx2ube5v5zpg.onion/?author=view&id=259) | { 0 }
 260 | Knuth | [Donald Ervin Knuth](http://c3jemx2ube5v5zpg.onion/?author=view&id=260) | { 0 }
 261 | Horstmann | [Ulrich Horstmann](http://c3jemx2ube5v5zpg.onion/?author=view&id=261) | { 0 }
 262 | Kirst | [Hans Hellmut Kirst](http://c3jemx2ube5v5zpg.onion/?author=view&id=262) | { 0 }
 263 | Frank | [Annelies Marie Frank](http://c3jemx2ube5v5zpg.onion/?author=view&id=263) | { 3 }
 264 | Hauswirth | [Mischa Hauswirth](http://c3jemx2ube5v5zpg.onion/?author=view&id=264) | { 0 }
 265 | Grey | [Aubrey David Nicholas Jasper de Grey](http://c3jemx2ube5v5zpg.onion/?author=view&id=265) | { 0 }
 266 | Smith | [J. Smith](http://c3jemx2ube5v5zpg.onion/?author=view&id=266) | { 0 }
 267 | Doerr | [Anthony Doerr](http://c3jemx2ube5v5zpg.onion/?author=view&id=267) | { 0 }
 268 | père | [Alexandre Dumas, père](http://c3jemx2ube5v5zpg.onion/?author=view&id=268) | { 2 }
 269 | Shakespeare | [William Shakespeare](http://c3jemx2ube5v5zpg.onion/?author=view&id=269) | { 0 }
 270 | Hawkins | [Paula Hawkins](http://c3jemx2ube5v5zpg.onion/?author=view&id=270) | { 0 }
 271 | Abdelilah-Bauer | [Barbara Abdelilah-Bauer](http://c3jemx2ube5v5zpg.onion/?author=view&id=271) | { 0 }
 272 | Corelli | [Marie Corelli](http://c3jemx2ube5v5zpg.onion/?author=view&id=272) | { 2 }
 273 | Archer | [Jeffrey Howard Archer](http://c3jemx2ube5v5zpg.onion/?author=view&id=273) | { 0 }
 274 | Orci | [Emma Magdolna Rozália Mária Jozefa Borbála Orczy de Orci](http://c3jemx2ube5v5zpg.onion/?author=view&id=274) | { 0 }
 275 | Sayers | [Dorothy Leigh Sayers](http://c3jemx2ube5v5zpg.onion/?author=view&id=275) | { 0 }
 276 | Fogel | [Karl Fogel](http://c3jemx2ube5v5zpg.onion/?author=view&id=276) | { 0 }
