Reading is learning. And we have schools to get started.
Languages are key to communication and so again to education.
So for better educational freedom we need especially books regularly used in schools.

You may **use the manual** [online](https://gitlab.com/lazy-book-crowd/jrc-manual/tree/master) (also as [PDF](http://c3jemx2ube5v5zpg.onion/readingclub-man.pdf))
to find out how to put your favorit book on [the readinclub](http://c3jemx2ube5v5zpg.onion/).


== Languages ==

feel free to add translation titles in here, right now there have been added:
+ en:English
+ de:Deutsch (German)
+ pl:Polski (Polish)
+ it:Italiano (Italian)
+ no:Norsk (Norwegian)
+ cz:Čeština (Czech)
+ gr:Ελληνικά (Greek)
+ nl:Nederlands (Dutch)
+ fr:Français (French)


== Format for this listing ==

List like:
* language short in 2 letters
* original title (translations titles, English one recommended if not the very same as original)
* writers name or pen name (may link to Jotunbanes)
  + {real name} if known and pen name identified
* if multiple languages, mark the done ones with ✓ for this languages title
* As soon as all asked translations are done, put it down on "done" section
  + add sources if any and where they can be found


== books with probably applying copyright == 

- de:**Die Physiker - eine Komödie in zwei Akten** (en:**The Physicists**), *Friedrich Dürrenmatt*
- de:**Der Besuch der alten Dame** (en:**The Visit**), *Friedrich Dürrenmatt*
- en:**Driving Miss Daisy**, *Alfred Uhry*
- de:**Das fliegende Klassenzimmer** (en:**The Flying Classroom**), *[Erich Kästner](http://c3jemx2ube5v5zpg.onion/?author=view&id=220)*
- de:**Andorra**, *Max Rudolf Frisch*
- de:**Kritik der reinen Vernunft** (en:**Critique of Pure Reason**), *Immanuel Kant*
- pl:**Akademia Pana Kleksa**, *Jan Brzechwa*
- en:**Charlie and the Chocolate Factory**, (pl:**Charlie i fabryka czekolady**, de:**Charlie und die Schokoladenfabrik**), *Roald Dahl*
- en:**The Wave** (de:**Die Welle**, it:**L'onda**), *Morton Rhue* {[Todd Strasser](http://c3jemx2ube5v5zpg.onion/?author=view&id=249)}
- de:✓[Die Judenbuche](http://c3jemx2ube5v5zpg.onion/?document=view&id=832&section=1) (en:**The Jew's Beech**), *[Annette von Droste-Hülshoff](http://c3jemx2ube5v5zpg.onion/?author=view&id=234&letter=D)* {Anna Elisabeth Franziska Adolphine Wilhelmine Louise Maria, Freiin von Droste zu Hülshoff}
- nl:✓[Het Achterhuis](http://c3jemx2ube5v5zpg.onion/?document=view&id=899) (de:[Tagebuch der Anne Frank](http://c3jemx2ube5v5zpg.onion/?document=view&id=903), en:[The Diary of a Young Girl](http://c3jemx2ube5v5zpg.onion/covers/cover900)), *[Anne Frank](http://c3jemx2ube5v5zpg.onion/?author=view&id=263)* {Annelies Marie Frank}
- de:✓[Rolltreppe abwärts](http://c3jemx2ube5v5zpg.onion/?document=view&id=853), [Hans-Georg Noack](http://c3jemx2ube5v5zpg.onion/?author=view&id=250)
- de:**Die Schachnovelle**, [Stefan Zweig](http://c3jemx2ube5v5zpg.onion/?author=view&id=244)
- de:✓[Die Blechtrommel](http://c3jemx2ube5v5zpg.onion/?document=view&id=778) {Danziger Triologie # 1} (en:✓[The Tin Drum](http://c3jemx2ube5v5zpg.onion/?document=view&id=813), pl:**Blaszany bębenek**), *[Günter Grass](http://c3jemx2ube5v5zpg.onion/?author=view&id=221)*
- de:✓[Katz und Maus](http://c3jemx2ube5v5zpg.onion/?document=view&id=835) {Danziger Triologie # 2} (en:**Cat and Mouse**), *[Günter Grass](http://c3jemx2ube5v5zpg.onion/?author=view&id=221)*
- de:✓[Hundejahre](http://c3jemx2ube5v5zpg.onion/?document=view&id=837) {Danziger Triologie # 3} (en:**Dog Years**), *[Günter Grass](http://c3jemx2ube5v5zpg.onion/?author=view&id=221)*
- de:✓[Das Parfum. Die Geschichte eines Mörders](http://c3jemx2ube5v5zpg.onion/?document=view&id=802) (en:✓[Perfume: The Story of a Murderer](http://c3jemx2ube5v5zpg.onion/?document=view&id=801), pl:**Pachnidło: Historia pewnego mordercy**, it:**Il profumo**), *[Patrick Süskind](http://c3jemx2ube5v5zpg.onion/?author=view&id=219)*
- de:✓[Wir Kinder vom Bahnhof Zoo](http://c3jemx2ube5v5zpg.onion/?document=view&id=825), *[Christiane F.](http://c3jemx2ube5v5zpg.onion/?author=view&id=193)* {Vera Christiane Felscherinow}
- en:✓[Lord of the Flies](http://c3jemx2ube5v5zpg.onion/?document=view&id=498) (de:**Herr der Fliegen**), *[William Gerald Golding](http://c3jemx2ube5v5zpg.onion/?author=view&id=115)*
- de:✓[Der Vorleser](http://c3jemx2ube5v5zpg.onion/?document=view&id=648) (en:✓[The Reader](http://c3jemx2ube5v5zpg.onion/?document=view&id=850), pl:**Lektor**), *[Bernhard Schlink](http://c3jemx2ube5v5zpg.onion/?author=view&id=189)*
- de:✓[Tintenherz](http://c3jemx2ube5v5zpg.onion/?document=view&id=646) {Tintenherz Triologie # 1} (en:✓[Inkheart](http://c3jemx2ube5v5zpg.onion/?document=view&id=662), pl:**Atramentowe serce**), *[Cornelia Maria Funke](http://c3jemx2ube5v5zpg.onion/?author=view&id=173)*
- de:Tintenblut {Tintenherz Triologie # 2} (en:**Inkspell**), *[Cornelia Maria Funke](http://c3jemx2ube5v5zpg.onion/?author=view&id=173)*
- de:Tintentod {Tintenherz Triologie # 3} (en:**Inkdeath**), *[Cornelia Maria Funke](http://c3jemx2ube5v5zpg.onion/?author=view&id=173)*
- en:✓[The Book Thief](http://c3jemx2ube5v5zpg.onion/?document=view&id=642), (de:✓[Die Bücherdiebin](http://c3jemx2ube5v5zpg.onion/?document=view&id=643), pl:**Złodziejka książek**), *[Markus Frank Zusak](http://c3jemx2ube5v5zpg.onion/?author=view&id=171)*
- de:✓[Krabat](http://c3jemx2ube5v5zpg.onion/?document=view&id=834) (en:✓[The Satanic Mill](http://c3jemx2ube5v5zpg.onion/?document=view&id=836), cz:**Čarodějův učeň**), *Otfried Preußler*
- de:✓[Im Westen nichts Neues](http://c3jemx2ube5v5zpg.onion/?document=view&id=775) (en:✓[All Quiet on the Western Front](http://c3jemx2ube5v5zpg.onion/?document=view&id=841), pl:**Na Zachodzie bez zmian**), *Erich Maria Remarque*
- de:✓[Homo faber. Ein Bericht](http://c3jemx2ube5v5zpg.onion/?document=view&id=819) (en:✓[Homo Faber. A Report](http://c3jemx2ube5v5zpg.onion/?document=view&id=817), pl:**Homo Faber: relacja**), *[Max Rudolf Frisch](http://c3jemx2ube5v5zpg.onion/?author=view&id=225)*
- de:**Der Seewolf**, *Jack London*
- de:**War Paul schuldig?: Kindheit und Jugend im Dritten Reich**, *Lisa Tetzner*
- de:**Die Dritte Macht**, *Perry Rhodan*

== possibly expired rights ==

Those are less important as they are or could be on [Gutenberg Project](https://en.wikipedia.org/wiki/Project_Gutenberg),
but still is a comparable low hanging fruit

- gr:**Αντιγόνη** (en:**Antigone**, any language), *Σοφοκλή* (en:*Sophocles*, de:*Sophokles*)
- it:**Commedia** (de:**Göttliche Komödie**, en:**Divine Comedy**), *Dante Alighieri*
- no:**Et dukkehjem** (en: **A Doll's House**, de:**Nora oder Ein Puppenheim**), *Hendrik Ibsen*
- de:**Faust** {series of two books}, *J.W. Goethe*
- en:✓[The Importance of Being Earnest, A Trivial Comedy for Serious People](https://c3jemx2ube5v5zpg.onion/?document=view&id=915), *[Oscar Wilde](http://c3jemx2ube5v5zpg.onion/?author=view&id=245)* {Oscar Fingal O'Flahertie Wills Wilde}
- en:**The Picture of Dorian Gray** (de:**Das Bildnis des Dorian Gray**), *[Oscar Wilde](http://c3jemx2ube5v5zpg.onion/?author=view&id=245)*
- de:**Nathan der Weise** (en:**Nathan the Wise**), *Gotthold Ephraim Lessing*
- de:**Emilia Galotti** (en), *Gotthold Ephraim Lessing*
- cz:**Osudy dobrého vojáka Švejka za světové války** (de:**Der brave Soldat Schwejk**, en:**The Good Soldier Švejk**), *Jaroslav Hašek*
- de:**Effi Briest** (en, pl), *Theodor Fontane*
- de:**Kabale und Liebe** (en:**Intrigue and Love**), *Johann Christoph Friedrich von Schiller*
- en:**The Secret Garden** (de:**Der Geheime Garten**, pl:**Tajemniczy ogród**), *Frances Hodgson Burnett*
- it:**Pinocchio** (de, en, pl), *Carlo Collodi*
- pl:**Historia żółtej ciżemki**, *Antonina Domańska*
- en:**The Tragedy of Macbeth** (pl:**Makbet**, de:**Macbeth**), *Shakespeare*
- en:**A Midsummer Night's Dream** (de:**Ein Sommernachtstraum**, pl:**Sen nocy letniej**), *Shakespeare*
- de:**Jedermann. Das Spiel vom Sterben des reichen Mannes** (en), *Hugo von Hofmannsthal*
- de:**Unterm Rad** (en:**Beneath the Wheel**, no:**Under hjulet**), *Hermann Karl Hesse*
- de:**Das Urteil** (en:✓[The Judgment](http://c3jemx2ube5v5zpg.onion/?document=view&id=612&section=10)), *[Franz Kafka](http://c3jemx2ube5v5zpg.onion/?author=view&id=151)*
- de:**Die Verwandlung** (en:✓[The Metamorphosis](http://c3jemx2ube5v5zpg.onion/?document=view&id=612&section=11), cz:**Proměna**), *[Franz Kafka](http://c3jemx2ube5v5zpg.onion/?author=view&id=151)*
- ch:**孫子兵法** (en:✓[The Art of War](http://c3jemx2ube5v5zpg.onion/?document=view&id=812), de:**Die Kunst des Krieges**), *[Sun Tzu](http://c3jemx2ube5v5zpg.onion/?author=view&id=120)*
- en:**The Tragedy of Macbeth** (de:**Macbeth**), *William Shakespeare*
- en:✓[Rome and Juliet](http://c3jemx2ube5v5zpg.onion/?document=view&id=907) (de:**Romeo & Julia**), *William Shakespeare*
- en:**Robinson Crusoe**, *Daniel Defoe*
- fr:**Les Trois Mousquetaires** {D'Artagnan # 1}  (en:✓[The Three Musketeers](http://c3jemx2ube5v5zpg.onion/?document=view&id=908)), *[Alexandre Dumas](http://c3jemx2ube5v5zpg.onion/?author=view&id=268)*
- fr:**Vingt ans après** {D'Artagnan # 2} (en:**Twenty Years After**), *[Alexandre Dumas](http://c3jemx2ube5v5zpg.onion/?author=view&id=268)*
- fr:**Le Vicomte de Bragelonne ou Dix ans plus tard** {D'Artagnan # 3} (en:**The Vicomte of Bragelonne: Ten Years Later**), *[Alexandre Dumas](http://c3jemx2ube5v5zpg.onion/?author=view&id=268)*

== done ==

Those books have been done with all requested translations.

Add a request and move up again if that translation is
available or even start one yourself to make it available.

- en:✓[Life of Pi](http://c3jemx2ube5v5zpg.onion/?document=view&id=629), *[Yann Martel](http://c3jemx2ube5v5zpg.onion/?author=view&id=165)*
  + de:✓[Schiffbruch mit Tiger](http://c3jemx2ube5v5zpg.onion/?document=view&id=630)
  + pl:✓[Życie Pi](http://c3jemx2ube5v5zpg.onion/?document=view&id=631)
  + it:✓[Vita di Pi](http://c3jemx2ube5v5zpg.onion/?document=view&id=633)
  + nl:✓[Het leven van Pi](http://c3jemx2ube5v5zpg.onion/?document=view&id=632)
