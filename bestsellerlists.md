We want to have [DRM](http://drm.info/) free ebooks.
So we aim where it might hurt.

In case you want to start reading or even (re)publish on the reading club
and do not know which book to take, these lists might give you an idea.

== Bestseller Lists ==

- [New York Times](http://www.nytimes.com/best-sellers-books/overview.html)
- [Amazon](http://www.amazon.co.uk/Best-Sellers-Books/zgbs/books)
- http://verywellsaid.com/england